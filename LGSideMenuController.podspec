Pod::Spec.new do |s|

    s.name = 'LGSideMenu'
    s.version = '2.1.1'
    s.platform = :ios, '8.0'
    s.license = 'MIT'
    s.homepage = 'https://bitbucket.org/omarjalil/lgsidemenu'
    s.author = { 'Grigory Lutkov' => 'Friend.LGA@gmail.com' }
    s.source = { :git => 'https://bitbucket.org/omarjalil/lgsidemenu.git' }
    s.summary = 'iOS view controller, shows left and right views by pressing button or gesture'

    s.requires_arc = true

    s.source_files = '*.{h,m}'

end